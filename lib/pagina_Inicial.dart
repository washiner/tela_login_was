
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class PaginaInicial extends StatefulWidget {


  @override
  _PaginaInicialState createState() => _PaginaInicialState();
}

class _PaginaInicialState extends State<PaginaInicial> {
  final tEmail = TextEditingController();
  final tSenha = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  var logado = "Deslogado";

  bool _success;
  String _userEmail;
  FirebaseAuth auth = FirebaseAuth.instance;




  void _register() async {
    final User user = (await
    auth.createUserWithEmailAndPassword(
      email: tEmail.text,
      password: tSenha.text,
    )
    ).user;
    if (user != null) {
      setState(() {
        _success = true;
        _userEmail = user.email;
      });
    } else {
      setState(() {
        _success = true;
      });
    }
  }





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        title: Text("Pagina Inicial"),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                FlutterLogo(
                  size: 100,
                ),
                TextFormField(
                  controller: tEmail,
                  keyboardType: TextInputType.emailAddress,
                  maxLength: 30,
                  style: TextStyle(
                      color: Colors.green,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      //border: InputBorder.none,
                      icon: Icon(Icons.email),
                      suffixIcon: Icon(Icons.email_outlined),
                      labelText: "Email",
                      labelStyle: TextStyle(
                        fontSize: 20,
                      ),
                      hintText: "Digite seu email"),
                      validator: (valorEmailDigitado) {
                        if (valorEmailDigitado.isEmpty)
                          return "O campo e obrigatorio";
                        if (!valorEmailDigitado.contains("@"))
                          return "Favor digite email valido";
                        if (valorEmailDigitado.length < 5) {
                          return "Digite no minimo 4 caracter";
                        }else{
                          return null;
                        }
                      }
                ),
                SizedBox(height: 20),
                TextFormField(
                  controller: tSenha,
                  keyboardType: TextInputType.number,
                  obscureText: true,
                  maxLength: 10,
                  style: TextStyle(
                      color: Colors.green,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    //border: InputBorder.none,
                    icon: Icon(Icons.login),
                    suffixIcon: Icon(Icons.login_outlined),
                    labelText: "Senha",
                    labelStyle: TextStyle(fontSize: 20),
                    hintText: "Digite sua senha",
                  ),
                  validator: (valorSenhaDigitado){
                    if(valorSenhaDigitado.isEmpty)
                    return "Senha Obrigatoria";
                    if(valorSenhaDigitado.length < 5) {
                      return "Minimo de 4 caracteres";
                    }else{
                      return null;
                    }
                  },
                ),
                SizedBox(height: 20),
                RaisedButton(
                    color: Colors.purple,
                    child: Text("Fazer Login", style: TextStyle(color: Colors.white)),
                    onPressed: () {
                      if(_formKey.currentState.validate())
                        _register();
                      setState(() {
                        return logado = "usuario logado com sucesso";
                      });
                    }
                    ),
                Text("${logado}"),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
